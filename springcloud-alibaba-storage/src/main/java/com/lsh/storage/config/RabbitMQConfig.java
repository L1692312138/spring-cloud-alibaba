package com.lsh.storage.config;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 * @author ：LiuShihao
 * @date ：Created in 2021/9/13 2:42 下午
 * @desc ：
 */
@Configuration
public class RabbitMQConfig {
    @Bean
    public Queue queue(){
        return new Queue("direct2");
    }
}
