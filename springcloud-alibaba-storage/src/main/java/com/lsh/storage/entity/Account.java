package com.lsh.storage.entity;

import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/3/22 4:09 下午
 * @desc ：账户表
 */
@Data
@Entity
@DynamicUpdate
@DynamicInsert
@Table(name = "account")
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**用户id*/
    private Integer userId;

    /**总额度*/
    private Integer total;

    /**已用额度*/
    private Integer used;

    /**剩余额度*/
    private Integer residue;
}
