package com.lsh.storage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.client.RestTemplate;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/6/18 5:10 下午
 * @desc ：库存服务启动类 9002
 */

@SpringBootApplication
@EnableDiscoveryClient
@ComponentScan("com.lsh.storage.*")
@EnableFeignClients(basePackages = {"com.lsh.storage.feign"})
//@EnableFeignClients(clients = {AccountApi.class, OrderApi.class, StorageApi.class})
public class StorageApplication {
    public static void main(String[] args) {
        SpringApplication.run(StorageApplication.class);
    }

    @Bean
    @LoadBalanced // ribbon注解负载均衡
    public RestTemplate getRestTemplate(){
        return new RestTemplate();
    }

}

