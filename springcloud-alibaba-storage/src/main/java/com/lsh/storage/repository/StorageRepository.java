package com.lsh.storage.repository;

import com.lsh.storage.entity.Storage;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/6/29 2:25 下午
 * @desc ：
 */
public interface StorageRepository extends JpaRepository<Storage,Integer> {
//    @Modifying
//    @Query(value = "    UPDATE storage SET used = used + :count,residue = residue - :count WHERE product_id = :productId",nativeQuery = true)
//    void decrease(@Param("productId") Integer productId, @Param("count") Integer count);

    Storage findTopByProductId (Integer productId);
}
