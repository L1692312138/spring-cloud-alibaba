package com.lsh.storage.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @Author LiuShihao
 * @Date 2021年06月29日12:33:12
 * @Desc Order服务 调用接口  springcloud-alibaba-order
 */
@Component
@FeignClient(value = "springcloud-alibaba-order")
public interface OrderApi {

    /**
     * 修改订单金额
     * @param userId
     * @param money
     * @param status
     * @return
     */
    @GetMapping("/order/update")
    String update(@RequestParam("userId") Integer userId, @RequestParam("money") Integer money, @RequestParam("status") Integer status);
}
