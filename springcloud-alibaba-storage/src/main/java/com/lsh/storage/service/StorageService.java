package com.lsh.storage.service;

import com.lsh.storage.entity.Storage;

import java.util.List;


/**
 * @author ：LiuShihao
 * @date ：Created in 2021/6/29 2:27 下午
 * @desc ：
 */
//@Transactional
public interface StorageService {

    List<Storage> findAll();

    /**
     * 扣减库存
     * @param productId
     * @param count
     */
    void decreaseStorage(Integer productId, Integer count);
}
