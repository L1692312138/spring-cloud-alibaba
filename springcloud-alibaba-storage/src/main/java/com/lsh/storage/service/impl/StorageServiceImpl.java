package com.lsh.storage.service.impl;

import com.lsh.storage.dao.StorageDao;
import com.lsh.storage.entity.Storage;
import com.lsh.storage.feign.OrderApi;
import com.lsh.storage.repository.StorageRepository;
import com.lsh.storage.service.StorageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/6/29 2:28 下午
 * @desc ：
 */
@Slf4j
@Service("StorageService")
public class StorageServiceImpl implements StorageService {
    @Autowired
    StorageRepository storageRepository;

    @Autowired
    OrderApi orderApi;

    @Autowired
    private StorageDao storageDao;


    //    @GlobalTransactional(name = "find-all-storage")
    @Override
    public List<Storage> findAll() {
        return storageRepository.findAll();
    }

    @Override
    public void decreaseStorage(Integer productId, Integer count) {
        log.info("------->扣减库存开始");
        storageDao.decrease(productId,count);
        log.info("------->扣减库存结束");

    }
}
