package com.lsh.account;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/6/18 5:10 下午
 * @desc ：账号模块服务 启动类
 */
@SpringBootApplication
@EnableDiscoveryClient
@ComponentScan("com.lsh.account.*")
//@EnableFeignClients(clients = {AccountApi.class, OrderApi.class, StorageApi.class})
@EnableFeignClients(basePackages = "com.lsh.account")
public class AccountApplication9001 {
    public static void main(String[] args) {
        SpringApplication.run(AccountApplication9001.class);
    }
}
