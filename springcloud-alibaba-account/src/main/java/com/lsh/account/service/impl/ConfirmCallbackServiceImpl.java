package com.lsh.account.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/9/2 3:54 下午
 * @desc ：
 *
 * 消息只要被 rabbitmq broker 接收到就会触发 confirmCallback 回调 。
 * correlationData：对象内部只有一个 id 属性，用来表示当前消息的唯一性。
 * ack：消息投递到broker 的状态，true表示成功。
 * cause：表示投递失败的原因。
 */
@Slf4j
@Component
public class ConfirmCallbackServiceImpl implements RabbitTemplate.ConfirmCallback {
    @Override
    public void confirm(CorrelationData correlationData, boolean ack, String cause) {
        if (!ack) {
            System.out.println("消息发送异常!");
        } else {
            //ack=true, cause=null
            System.out.println("CorrelationData:"+correlationData);
            System.out.println("broker已经收到确认,ack="+ack+", cause="+cause);
        }



    }
}
