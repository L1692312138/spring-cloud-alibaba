package com.lsh.account.service;

import com.lsh.account.entity.Account;

import java.util.List;

/**
 * @author LiuShihao
 */

public interface AccountService {

    /**
     * 扣减账户余额
     * @param userId 用户id
     * @param money 金额
     */
    void decrease(Integer userId, Integer money);

    /**
     * 查询所有哦账户信息
     * @return
     */
    List<Account> findAll();
    /**
     * 查询所有哦账户信息
     * @return
     */
    List<Account> findAllByMyBatis();


    Object findAllStorage();

}
