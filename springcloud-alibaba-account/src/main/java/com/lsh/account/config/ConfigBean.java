package com.lsh.account.config;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/7/23 10:47 上午
 * @desc ：
 */
//加上注解@Configuration,声明该类是一个配置类,相等于是Spring的配置文件
@Configuration
public class ConfigBean {
    //加注解@Bean 就相当于是在Spring的配置文件中写bean标签
    @Bean
    @LoadBalanced
    public RestTemplate getRestTemplate(){
        System.out.println("------return new RestTemplat------");
        return new RestTemplate();
    }
}
