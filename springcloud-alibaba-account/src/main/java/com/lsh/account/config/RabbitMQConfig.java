package com.lsh.account.config;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/9/2 9:47 上午
 * @desc ：
 */
@Configuration
public class RabbitMQConfig {

    /**
     * 创建队列  直接模式
     * @return
     */
    @Bean("queue1")
    public Queue queue1() {
        // 队列名
        return new Queue("direct");
    }

    /**
     *  分裂模式
     * @return
     */
    @Bean("fanout1")
    public Queue fanout1() {
        // 队列名
        return new Queue("fanout1");
    }
    /**
     *  分裂模式
     * @return
     */
    @Bean("fanout2")
    public Queue fanout2() {
        // 队列名
        return new Queue("fanout2");
    }
    /**
     * 分裂模式
     * 创建交换机
     */
    @Bean
    public FanoutExchange fanoutExchange() {
        // 交换机名字
        return new FanoutExchange("fanoutExchange");
    }
    /**
     * 分裂模式
     * 队列绑定到交换机上
     */
    @Bean
    public Binding bingdingFanout1(@Qualifier("fanout1") Queue q1, FanoutExchange fanoutExchange){
        return BindingBuilder.bind(q1).to(fanoutExchange);
    }
    @Bean
    public Binding bingdingFanout2(@Qualifier("fanout2") Queue q2, FanoutExchange fanoutExchange){
        return BindingBuilder.bind(q2).to(fanoutExchange);
    }

    /**
     * 创建队列  主题模式
     * @return
     */
    @Bean("topic1")
    public Queue topic1() {
        // 队列名1
        return new Queue("topic1");
    }
    /**
     * 创建队列  主题模式
     * @return
     */
    @Bean("topic2")
    public Queue topic2() {
        // 队列名2
        return new Queue("topic2");
    }
    /**
     * 创建队列  主题模式
     * @return
     */
    @Bean("topic3")
    public Queue topic3() {
        // 队列名2
        return new Queue("topic3");
    }

    /**
     * 主题模式
     * 创建交换机
     */
    @Bean
    public TopicExchange topicExchange() {
        // 交换机名字
        return new TopicExchange("topicExchange");
    }

    /**
     * 队列绑定到交换机上,并且指定routingKey
     * “#”表示0个或若干个关键字，“”表示一个关键字。如“log.”能与“log.warn”匹配，无法与“log.warn.timeout”匹配；但是“log.#”能与上述两者匹配。
     */
    @Bean
    public Binding bingdingTopic1(@Qualifier("topic1") Queue myQueue1, TopicExchange topicExchange){
        return BindingBuilder.bind(myQueue1).to(topicExchange).with("orders.#");
    }
    @Bean
    public Binding bingdingTopic2(@Qualifier("topic2") Queue myQueue2, TopicExchange topicExchange){
        return BindingBuilder.bind(myQueue2).to(topicExchange).with("#.log");
    }
    @Bean
    public Binding bingdingTopic3(@Qualifier("topic3")Queue myQueue3,TopicExchange topicExchange){
        return BindingBuilder.bind(myQueue3).to(topicExchange).with("user.log");
    }

    /**
     *
     *
     * 消息-->进入delayExchange交换机-->delayQueue延时队列 5s未消费-->进入deadLetterExchange死信交换机-->进入死信队列deadLetterQueue
     * @return
     */

    // 创建交换机
    @Bean("delayExchange")
    public DirectExchange delayExchange(){
        return new DirectExchange("delayExchange");
    }

    // 创建交换机
    @Bean("deadLetterExchange")
    public DirectExchange deadLetterExchange(){
        return new DirectExchange("deadLetterExchange");
    }

    /**
     * 死信队列
     * @return
     */
    @Bean("deadLetterQueue")
    public Queue deadLetterQueueA(){
        return new Queue("deadLetterQueue");
    }

    /**
     * 创建延时队列
     * 并绑定到对应的死信交换机
     * @return
     */
    @Bean("delayQueue")
    public Queue delayQueue(){
        Map<String, Object> args = new HashMap<>(2);
        // x-dead-letter-exchange    这里声明当前队列绑定的死信交换机
        args.put("x-dead-letter-exchange", "deadLetterExchange");
        // x-dead-letter-routing-key  这里声明当前队列的死信路由key
        args.put("x-dead-letter-routing-key", "deadLetterQueueAroutingkey");
        // x-message-ttl  声明队列的TTL  单位是毫秒
        args.put("x-message-ttl", 5000);
        return QueueBuilder.durable("delayQueue").withArguments(args).build();
    }

    /**
     * 将延时队列与延时交换机绑定   并指定延时队列路由
     * @param queue
     * @param exchange
     * @return
     */
    @Bean
    public Binding delayBindingA(@Qualifier("delayQueue") Queue queue,
                                 @Qualifier("delayExchange") DirectExchange exchange){
        return BindingBuilder.bind(queue).to(exchange).with("delayQueueAroutingkey");
    }

    /**
     * 将普通队列 与 交换机绑定   指定队列路由
     * @param queue
     * @param exchange
     * @return
     */
    @Bean
    public Binding deadLetterBindingA(@Qualifier("deadLetterQueue") Queue queue,
                                      @Qualifier("deadLetterExchange") DirectExchange exchange){
        return BindingBuilder.bind(queue).to(exchange).with("deadLetterQueueAroutingkey");
    }







}
