package com.lsh.account.controller;

import com.lsh.account.service.impl.ConfirmCallbackServiceImpl;
import com.lsh.account.service.impl.ReturnCallbackServiceImpl;
import com.lsh.common.util.ResultObject;
import com.lsh.common.util.StatusCode;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/9/2 9:49 上午
 * @desc ：
 * Provider  提供者
 * Producer  生产者
 */
@RestController
@RequestMapping("/mq")
public class MqProductController {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    ConfirmCallbackServiceImpl confirmCallbackService;

    @Autowired
    ReturnCallbackServiceImpl returnCallbackService;

    /**
     *
     * 生产消息到队列
     * @param
     * @return
     */
    @GetMapping("/sendDirect")
    public ResultObject sendDirect(String routingKey,String msg){

        try {
            /**
             * 确保消息发送失败后可以重新返回到队列中
             * 注意：yml需要配置 publisher-returns: true
             */
            rabbitTemplate.setMandatory(true);

            /**
             * 消费者确认收到消息后，手动ack回执回调处理
             */
            rabbitTemplate.setConfirmCallback(confirmCallbackService);

            /**
             * 消息投递到队列失败回调处理
             */
            rabbitTemplate.setReturnCallback(returnCallbackService);

            rabbitTemplate.convertAndSend(routingKey,msg);
        }catch (Exception e){
            return new ResultObject(true, StatusCode.ERROR, e.getMessage());
        }

        return new ResultObject(true, StatusCode.OK, "路由Key："+routingKey+"，直接模式投递消息成功");
    }
    /**
     *
     * 生产消息到队列
     * @param msg
     * @return
     */
    @GetMapping("/sendFanout")
    public ResultObject sendFanout(String msg){
        try {
            /**
             * fanoutExchange  指定消息进入的交换机的名字
             * 分列模式不需要使用routing
             */
            rabbitTemplate.convertAndSend("fanoutExchange",null,msg);
        }catch (Exception e){
            return new ResultObject(true, StatusCode.ERROR, e.getMessage());
        }

        return new ResultObject(true, StatusCode.OK, "路由Key：fanoutExchange，分裂模式投递消息成功");
    }
    /**
     *
     * 生产消息到队列
     * @param msg
     * @return
     */
    @GetMapping("/sendTopic")
    public ResultObject sendTopic(String routingKey,String msg){
        try {
            /**
             * topicExchange  指定消息进入的交换机的名字
             * routingKey     指定该条消息的routingKey -> 即匹配规则
             */
            rabbitTemplate.convertAndSend("topicExchange",routingKey,msg);
        }catch (Exception e){
            return new ResultObject(true, StatusCode.ERROR, e.getMessage());
        }

        return new ResultObject(true, StatusCode.OK, "路由Key：topicExchange，路由规则："+routingKey+"，主题模式投递消息成功");
    }
    @GetMapping("/sendDelay")
    public ResultObject sendDelay(){
        try {
            Date date = new Date();

            System.out.println("消息已发送："+date.toString());
            rabbitTemplate.convertAndSend("delayExchange","delayQueueAroutingkey",date.toString());
        }catch (Exception e){
            return new ResultObject(true, StatusCode.ERROR, e.getMessage());
        }

        return new ResultObject(true, StatusCode.OK, "路由Key：delayQueueAroutingkey，交换机：delayExchange，延时投递成功");
    }





}
