package com.lsh.account.controller;

import com.lsh.account.service.AccountService;
import com.lsh.common.util.ResultObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/7/28 8:05 下午
 * @desc ：
 */
@RestController
public class AccountRestTemplateController {

    @Autowired
    AccountService accountService;


    @GetMapping("/account/get")
    public ResultObject getFindAll(@RequestParam String name,@RequestParam Integer age){
        System.out.println("getFindAll入参：name="+name+",age:"+age);
        return new ResultObject(true,0000,"Account服务GET方法",accountService.findAll());
    }


    @PostMapping("/account/postKeyValue")
    public ResultObject postKeyValueFindAll(String name,Integer age){
        System.out.println("postKeyValue入参：name : "+name+", age : "+age);
        return new ResultObject(true,0000,"Account服务POST方法",accountService.findAll());
    }

    @PostMapping("/account/postJson")
    public ResultObject postJsonFindAll(@RequestBody Map<String,Object> param){
        System.out.println("postJson入参："+param);
        return new ResultObject(true,0000,"Account服务POST方法",accountService.findAll());
    }
    @PutMapping("/account/putKeyValue")
    public ResultObject putKeyValueFindAll(String name,Integer age){
        System.out.println("putKeyValue：name : "+name+", age : "+age);
        return new ResultObject(true,0000,"Account服务PUT方法",name+":"+age);
    }
    @PutMapping("/account/putJson")
    public ResultObject putJsonFindAll(@RequestBody Map<String,Object> param){
        System.out.println("putJson："+param);
        return new ResultObject(true,0000,"Account服务PUT方法",param);

    }
    @DeleteMapping("/account/del")
    public ResultObject delFindAll(String name,Integer age){
        System.out.println("Account服务DELETE方法入参：name : "+name+", age : "+age);
        return new ResultObject(true,0000,"Account服务DELETE方法",name+", age : "+age);
    }

    @DeleteMapping("/account/del/{id}")
    public ResultObject delFindAllPathVariable(@PathVariable("id") Integer id){
        System.out.println("Account服务DELETE方法入参："+id);
        return new ResultObject(true,0000,"Account服务DELETE方法",id);
    }
}
