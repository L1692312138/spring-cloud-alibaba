package com.lsh.account.repository;

import com.lsh.account.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/6/29 10:38 上午
 * @desc ：
 */
@Repository
public interface AccountRepository extends JpaRepository<Account,Integer> {
//    @Modifying
//    @Query(value = "UPDATE  account SET  used = used + :money ,residue = residue - :money WHERE  user_id = :userId",nativeQuery = true)
//    void decrease(@Param("userId") int userId, @Param("money") Integer money);

    Account findTopByUserId (Integer userId);
}
