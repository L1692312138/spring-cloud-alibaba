package com.lsh.account.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/6/29 2:51 下午
 * @desc ：springcloud-alibaba-account
 */
@Component
@FeignClient(name = "springcloud-alibaba-account")
public interface AccountApi {
    /**
     * 查询所有账户数据
     * @return
     */
    @GetMapping("/account/findAll")
    String findAll();

    /**
     * 扣减账户
     * @param userId
     * @param money
     * @return
     */
    @GetMapping("/account/decrease")
    String decreaseAccount(@RequestParam("userId") Integer userId, @RequestParam("money") Integer money);

}
