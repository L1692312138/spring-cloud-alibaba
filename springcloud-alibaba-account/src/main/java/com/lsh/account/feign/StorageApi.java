package com.lsh.account.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/6/29 2:27 下午
 * @desc ：
 */
@Component
@FeignClient(value = "springcloud-alibaba-storage")
public interface StorageApi {

    /**
     * 查询全部库存数据
     * @return
     */
    @GetMapping("/storage/findAll")
    String findAll();

    /**
     * 扣减库存
     * @param productId
     * @param count
     * @return
     */
    @GetMapping("/storage/decrease")
    String decreaseStorage(@RequestParam("productId") Integer productId, @RequestParam("count") Integer count);


    @GetMapping("/storage/accountToStorage")
    String accountToStorage();
}
