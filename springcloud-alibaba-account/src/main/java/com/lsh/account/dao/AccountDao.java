package com.lsh.account.dao;

import com.lsh.account.entity.Account;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author IT云清
 */
@Mapper
public interface AccountDao {

    /**
     * 扣减账户余额
     * @param userId 用户id
     * @param money 金额
     */
    void decrease(@Param("userId") Integer userId, @Param("money") Integer money);


    @Select("SELECT * FROM account ")
    List<Account> findAllByMyBatis();
}
