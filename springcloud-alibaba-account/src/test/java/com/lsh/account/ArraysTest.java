package com.lsh.account;

import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/9/6 10:07 上午
 * @desc ：
 */
@SpringBootTest
public class ArraysTest {

    @Test
    public void test (){
        String[] a = new String[]{"a","b","3"};
        String[] b = new String[]{"a","b","3"};
        String[] c = a;
        System.out.println(a == b);
        System.out.println(a == c);
        double [] e = {0.1,0.2,0.3};
        double [] f = {0.1,0.2,0.3};
        //False because the two arrays are different objects ,even though they are identical
        System.out.println(e == f);

    }

}
