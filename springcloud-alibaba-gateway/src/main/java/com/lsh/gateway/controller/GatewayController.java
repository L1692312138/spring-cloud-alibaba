package com.lsh.gateway.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/7/16 9:57 上午
 * @desc ：
 */
@RestController
public class GatewayController {

    @GetMapping("/test")
    public Map testApi() throws InterruptedException {
        HashMap<String, String> map = new HashMap<>();
        map.put("code","0000");
        map.put("msg","success");
        map.put("api","/api/test");
//        Thread.sleep(2000);
        return map;

    }

    @RequestMapping(value = "/gatewayHystrix",produces = "text/html;charset=UTF-8")
    public String hystrix(){
        System.out.println("执行Gateway降级方法");
        String body = "<html><body><div style='width:800px; margin:auto; text-align:center; font-size:24px' >服务器繁忙，请稍后再试！ </div></body></html>";
        return body;

    }
}
