package com.lsh.gateway.dto;

import lombok.Data;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/8/17 10:48 上午
 * @desc ：
 */

@Data
public class GatewayPredicateDefinition {
    //断言对应的Name
    private String name;
    //配置的断言规则
    private Map<String, String> args = new LinkedHashMap<>();
}
