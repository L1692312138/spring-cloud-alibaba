package com.lsh.gateway.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableReactiveMethodSecurity;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.server.SecurityWebFilterChain;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/8/18 11:49 上午
 * @desc ：
 * 从Spring Cloud Gateway文档中：
 * Spring Cloud Gateway需要Spring Boot和Spring Webflux提供的Netty运行时。它不能在传统的Servlet容器中工作，也不能在构建为WAR时工作。
 * 扩展WebSecurityConfigurerAdapter是为了基于servlet的应用程序。
 * 您应该为 reactive 应用程序配置Spring Security。
 */
@Configuration
@EnableWebFluxSecurity
@EnableReactiveMethodSecurity
public class WebSecurityConfig  {

    @Bean
    public BCryptPasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }


    @Bean
    public SecurityWebFilterChain springSecurityFilterChain(ServerHttpSecurity http) {
        http.csrf().disable();
        http
                .authorizeExchange()
                .anyExchange().permitAll()
                .and()
                .formLogin()
        ;

        return http.build();
    }


}
