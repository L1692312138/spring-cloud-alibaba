package com.lsh.oauth2.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/8/18 8:20 下午
 * @desc ：
 */
@Data
@Entity
@Table(name = "sys_role_permission")
public class SysRolePermission {
    @Id
    private Integer roleId;

    private Integer permissionId;

}
