package com.lsh.oauth2.repository;

import com.lsh.oauth2.entity.SysPermission;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/8/18 8:07 下午
 * @desc ：
 */
public interface SysPermissionRepository extends JpaRepository<SysPermission,Integer> {

    //根据权限ID查询权限
    List<SysPermission> findSysPermissionsByIdIn(List<Integer> ids);
}
