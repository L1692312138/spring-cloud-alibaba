package com.lsh.oauth2.repository;

import com.lsh.oauth2.entity.SysOperLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/9/17 2:09 下午
 * @desc ：
 */
public interface SysOperLogRepository extends JpaRepository<SysOperLog,Integer>, JpaSpecificationExecutor<SysOperLog> {

    /**根据IDs 批量删除*/
    void deleteByOperIdIn (Integer[] ids);
    /**根据id查询详情*/
    SysOperLog findByOperId(Integer id);
}
