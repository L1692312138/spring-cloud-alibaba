package com.lsh.oauth2.repository;

import com.lsh.oauth2.entity.SysLogininfor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/9/17 2:07 下午
 * @desc ：用户登录信息记录
 */
public interface SysLogininforRepository extends JpaRepository<SysLogininfor,Integer> {

    /**根据用户名  分页查询*/
    Page<SysLogininfor> findSysLogininforsByUserName (String username, Pageable page);

    /**根据Id 批量删除*/
    void deleteByInfoIdIn (Integer[] ids);

}
