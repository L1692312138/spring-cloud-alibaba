package com.lsh.oauth2.repository;

import com.lsh.oauth2.entity.SysRole;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/8/18 8:06 下午
 * @desc ：
 */
public interface SysRoleRepository extends JpaRepository<SysRole,Integer> {

    //根据角色ID查询角色
    List<SysRole> findSysRolesByIdIn(List<Integer> ids);

    //根据角色Name查询角色
    SysRole findTopByName(String roleName);

}
