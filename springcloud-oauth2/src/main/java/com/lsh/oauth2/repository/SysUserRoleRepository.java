package com.lsh.oauth2.repository;

import com.lsh.oauth2.entity.SysUserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/8/18 8:21 下午
 * @desc ：用户-角色关联表
 */
public interface SysUserRoleRepository extends JpaRepository<SysUserRole,Integer> {
    //根据用户ID查询对应角色ID
    @Query(value = "SELECT role_id FROM sys_user_role WHERE user_id = ?",nativeQuery = true)
    List<Integer> findRoleIdsByUserId(Integer userId);


}
