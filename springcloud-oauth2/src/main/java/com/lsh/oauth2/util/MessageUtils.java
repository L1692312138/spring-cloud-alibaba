package com.lsh.oauth2.util;

import com.lsh.oauth2.util.spring.SpringUtils;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;

/**
 * 获取i18n资源文件
 * 国际化信息目录：/i18n/messages
 * 中文文件  messages_zh_CN.properties
 * 英文文件  messages_en_US.properties
 *
 * 设置中文 ：   LocaleContextHolder.getLocale()、Locale.SIMPLIFIED_CHINESE
 * 设置英文：    Locale.US
 * @author ruoyi
 */
public class MessageUtils {
//    @Autowired
//    MessageSource messageSource;
    /**
     * 根据消息键和参数 获取消息 委托给spring messageSource
     *
     * @param code 消息键
     * @param args 参数
     * @return 获取国际化翻译值
     */
    public static String message(String code, Object... args) {
        MessageSource messageSource = SpringUtils.getBean(MessageSource.class);
        //LocaleContextHolder.getLocale() 中文：Locale.SIMPLIFIED_CHINESE 英文： Locale.US
        //params look like "{0}", "{1,date}", "{2,time}" within a message
        return messageSource.getMessage(code, args, LocaleContextHolder.getLocale());
    }
}
