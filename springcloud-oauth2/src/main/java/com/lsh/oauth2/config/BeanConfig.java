package com.lsh.oauth2.config;

import com.lsh.common.redis.RedisCache;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/9/15 9:04 上午
 * @desc ：
 */
@Configuration
public class BeanConfig {
    /**
     * Redis工具类
     * @return
     */
    @Bean
    public RedisCache redisCache(){
        return new RedisCache();
    }

    /**
     * RestTemplate
     * @return
     */
    @Bean
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }
}
