package com.lsh.oauth2.service.impl;

import com.lsh.oauth2.entity.SysLogininfor;
import com.lsh.oauth2.repository.SysLogininforRepository;
import com.lsh.oauth2.service.ISysLogininforService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

/**
 * 系统访问日志情况信息 服务层处理
 * 
 * @author ruoyi
 */
@Service
public class SysLogininforServiceImpl implements ISysLogininforService {

    @Autowired
    SysLogininforRepository sysLogininforRepository;

    /**
     * 新增系统登录日志
     * 
     * @param logininfor 访问日志对象
     */
    @Override
    public void insertLogininfor(SysLogininfor logininfor) {
        sysLogininforRepository.save(logininfor);
    }

    /**
     * 查询系统登录日志集合
     * 
     * @param logininfor 访问日志对象
     * @return 登录记录集合
     */
    @Override
    public Page<SysLogininfor> selectLogininforList(SysLogininfor logininfor) {
        PageRequest of = PageRequest.of(logininfor.getPageNum()-1,logininfor.getPageSize());
        return sysLogininforRepository.findSysLogininforsByUserName(logininfor.getUserName(),of);
    }

    /**
     * 批量删除系统登录日志
     * 
     * @param infoIds 需要删除的登录日志ID
     * @return
     */
    @Override
    public void deleteLogininforByIds(Integer[] infoIds) {
        sysLogininforRepository.deleteByInfoIdIn(infoIds);
    }

    /**
     * 清空系统登录日志
     */
    @Override
    public void cleanLogininfor() {
        sysLogininforRepository.deleteAll();
    }
}
