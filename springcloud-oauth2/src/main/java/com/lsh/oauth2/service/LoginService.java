package com.lsh.oauth2.service;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/9/17 10:10 上午
 * @desc ：
 */
public interface LoginService {
    /**登录接口*/
    String login(String username, String password, String code, String uuid);
}
