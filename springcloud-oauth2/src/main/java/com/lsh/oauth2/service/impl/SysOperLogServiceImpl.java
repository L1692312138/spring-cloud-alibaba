package com.lsh.oauth2.service.impl;

import com.lsh.common.util.StringUtils;
import com.lsh.oauth2.entity.SysOperLog;
import com.lsh.oauth2.repository.SysOperLogRepository;
import com.lsh.oauth2.service.ISysOperLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;

/**
 * 操作日志 服务层处理
 * 
 * @author ruoyi
 */
@Service
public class SysOperLogServiceImpl implements ISysOperLogService {

    @Autowired
    SysOperLogRepository sysOperLogRepository;

    /**
     * 新增操作日志
     * 
     * @param operLog 操作日志对象
     */
    @Override
    public void insertOperlog(SysOperLog operLog) {
        sysOperLogRepository.save(operLog);
    }

    /**
     * 查询系统操作日志集合
     * 
     * @param operLog 操作日志对象
     * @return 操作日志集合
     */
    @Override
    public Page<SysOperLog> selectOperLogList(SysOperLog operLog) {

        PageRequest of = PageRequest.of(operLog.getPageNum() - 1, operLog.getPageSize());
        Page<SysOperLog> all = sysOperLogRepository.findAll((Specification<SysOperLog>) (root, criteriaQuery, criteriaBuilder) -> {
            ArrayList<Predicate> list = new ArrayList<>();
            // 操作模块
            if (StringUtils.isNotEmpty(operLog.getTitle())){
                Predicate predicate = criteriaBuilder.equal(root.get("title").as(String.class), operLog.getTitle());
                list.add(predicate);
            }
            //业务类型（0其它 1新增 2修改 3删除）
            if (StringUtils.isNotNull(operLog.getBusinessType())){
                Predicate predicate = criteriaBuilder.equal(root.get("businessType").as(String.class), operLog.getTitle());
                list.add(predicate);
            }
            //请求方法
            if (StringUtils.isNotEmpty(operLog.getMethod())){
                Predicate predicate = criteriaBuilder.equal(root.get("method").as(String.class), operLog.getTitle());
                list.add(predicate);
            }
            //请求方法
            if (StringUtils.isNotEmpty(operLog.getRequestMethod())){
                Predicate predicate = criteriaBuilder.equal(root.get("requestMethod").as(String.class), operLog.getTitle());
                list.add(predicate);
            }
            //操作类别（0其它 1后台用户 2手机端用户
            if (StringUtils.isNotNull(operLog.getOperatorType())){
                Predicate predicate = criteriaBuilder.equal(root.get("operatorType").as(String.class), operLog.getTitle());
                list.add(predicate);
            }
            //操作人员
            if (StringUtils.isNotEmpty(operLog.getOperName())){
                Predicate predicate = criteriaBuilder.equal(root.get("operName").as(String.class), operLog.getTitle());
                list.add(predicate);
            }
            //部门名称
            if (StringUtils.isNotEmpty(operLog.getDeptName())){
                Predicate predicate = criteriaBuilder.equal(root.get("deptName").as(String.class), operLog.getTitle());
                list.add(predicate);
            }
            //操作地址 IP地址
            if (StringUtils.isNotEmpty(operLog.getOperIp())){
                Predicate predicate = criteriaBuilder.equal(root.get("operIp").as(String.class), operLog.getTitle());
                list.add(predicate);
            }
            //操作地点
            if (StringUtils.isNotEmpty(operLog.getOperLocation())){
                Predicate predicate = criteriaBuilder.equal(root.get("operLocation").as(String.class), operLog.getTitle());
                list.add(predicate);
            }
            Predicate[] predicates = list.toArray(new Predicate[list.size()]);
            return criteriaBuilder.and(predicates);
        }, of);

        return all;
    }

    /**
     * 批量删除系统操作日志
     * 
     * @param operIds 需要删除的操作日志ID
     * @return 结果
     */
    @Override
    public void deleteOperLogByIds(Integer[] operIds) {
         sysOperLogRepository.deleteByOperIdIn(operIds);
    }

    /**
     * 查询操作日志详细
     * 
     * @param operId 操作ID
     * @return 操作日志对象
     */
    @Override
    public SysOperLog selectOperLogById(Integer operId) {
        return sysOperLogRepository.findByOperId(operId);
    }

    /**
     * 清空操作日志
     */
    @Override
    public void cleanOperLog() {
        sysOperLogRepository.deleteAll();
    }
}
