# SpringSecurityOAuth2.0
分布式系统认证授权解决方案
- 授权码模式： response_type=code ；  四种中最安全的
- 密码模式：  grant_type=password ；直接申请令牌，参数携带用户名和密码,需要将用户名密码泄露给客户端
- 简化模式： response_type=token； 授权认证通过后直接返回令牌，不需要再次请求申请令牌接口
- 客户端模式：客户端Id、客户端秘钥


# 授权模式
## 1. 授权码模式
1. 用户打开客户端，客户端请求用户授权它将浏览器重定向到授权服务器，此时需要SpringSecurity登录认证<br>
`http://localhost:8081/oauth2/oauth/authorize?client_id=c1&client_secret=secret&response_type=code&scope=all&redirect_uri=http://www.baidu.com`<br>
登出：http://localhost:8081/oauth2/login?logout
<table>
<tr>
<td>客户端ID标识</td>
<td>客户端秘钥</td>
<td>授权模式</td>
<td>权限范围</td>
<td>重定向URL</td>
</tr>
<tr>
<td>client_id</td>
<td>client_secret</td>
<td>response_type</td>
<td>scope</td>
<td>redirect_uri</td>
</tr>
<tr>
<td>c1</td>
<td>secret</td>
<td>code</td>
<td>all</td>
<td>http://www.baidu.com</td>
</tr>
</table>

2. 浏览器出现授权服务器授权页面，用户同意授权
用户同意授权，跳转页面，授权码会拼接在URL后：`https://www.baidu.com/?code=HnqLzS`
<table>
<tr>
<td>授权码</td>
</tr>
<tr>
<td>code</td>
</tr>
<tr>
<td>HnqLzS</td>
</tr>
</table>

3. 授权服务器将授权码(Authorization_Code)转经浏览器发送给客户端
申请令牌：`http://localhost:8081/oauth2/oauth/token`
POST请求：
<table>
<tr>
<td>客户端ID标识</td>
<td>客户端秘钥</td>
<td>授权模式</td>
<td>授权码</td>
<td>重定向URL</td>
</tr>
<tr>
<td>client_id</td>
<td>client_secret</td>
<td>grant_type</td>
<td>code</td>
<td>redirect_uri</td>
</tr>
<tr>
<td>c1</td>
<td>secret</td>
<td>authorization_code</td>
<td>HnqLzS</td>
<td>http://www.baidu.com</td>
</tr>
</table>


注意：此处申请令牌时的参数配置需要和`授权服务器`配置的`客户端详情信息`一致，且重定向地址需要个请求授权码时的重定向地址一样。

授权服务器返回令牌`access_token`：
```
{
    "access_token": "cf00764b-e516-4de0-9721-ea6644e814f3",
    "token_type": "bearer",
    "refresh_token": "a2f1244f-e16a-4bb9-ba4e-14448753cc26",
    "expires_in": 7199,
    "scope": "all"
}
```

4. 客户端通过授权码向授权服务器申请令牌access_token
5. 授权服务器返回令牌
## 2. 密码模式

不需要申请授权码，直接携带用户名密码申请令牌：http://localhost:8081/oauth2/oauth/token
<table>
<tr>
<td>客户端ID标识</td>
<td>客户端秘钥</td>
<td>授权模式</td>
<td>授权码</td>
<td>重定向URL</td>
</tr>
<tr>
<td>client_id</td>
<td>client_secret</td>
<td>grant_type</td>
<td>username</td>
<td>password</td>
<td>redirect_uri</td>
</tr>
<tr>
<td>c1</td>
<td>secret</td>
<td>password</td>
<td>zs</td>
<td>123</td>
<td>http://www.baidu.com</td>
</tr>
</table>

## 3. 简单模式

## 4. 客户端模式


# RSA非对称加密算法


创建一个目录，在目录下执行命令：

keytool -genkeypair -alias com.lsh.rsa -keyalg RSA -keypass rsapassword -keystore rsa_first.jks -storepass rsapassword

-alias:密钥的别名  com.lsh.rsa<br>
-keyalg:使用的hash算法  RSA<br>
-keypass:密钥的访问密码 rsapassword <br>
-keystore:密钥库文件名，rsa_first.jks 保存了生成的证书 <br>
-storepass:密钥库的访问密码  rsapassword <br>

查看证书：
keytool -list -keystore rsa_first.jks

[https://blog.csdn.net/DreamsArchitects/article/details/109674979](https://blog.csdn.net/DreamsArchitects/article/details/109674979)
