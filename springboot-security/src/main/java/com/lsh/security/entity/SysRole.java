package com.lsh.security.entity;

import lombok.Data;

import javax.persistence.*;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/8/18 8:02 下午
 * @desc ：
 */
@Data
@Entity
@Table(name = "sys_role")
public class SysRole {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

}
