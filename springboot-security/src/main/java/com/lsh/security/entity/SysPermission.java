package com.lsh.security.entity;

import lombok.Data;

import javax.persistence.*;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/8/18 8:02 下午
 * @desc ：
 */
@Data
@Entity
@Table(name = "sys_permission")
public class SysPermission {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String url;

    private String permission; // 权限

//    @Transient
//    private List permissions; // 存储根据,拆分后的权限集合
//
//    public List getPermissions() {
//        return Arrays.asList(this.permission.trim().split(","));
//    }

}
