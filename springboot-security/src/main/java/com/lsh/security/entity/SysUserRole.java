package com.lsh.security.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/8/18 8:14 下午
 * @desc ：
 */
@Data
@Entity
@Table(name = "sys_user_role")
public class SysUserRole {
    @Id
    private Integer userId;

    private Integer roleId;

}
