@[TOC](目录)


#SpringBoot Security DEMO

# 自定义登录页面
在spring security中配置自定义登录的时候，登录请求方式必须为POST
Security默认登陆认证页面，可以修改默认登陆页面
在于resource目录下创建/view/login.html ，表单登录路径为login
注意：（静态资源需要放在 `resources/static` 才会放行）
在application.yml配置`视图解析器`
```
spring:
  mvc:
    # 视图解析器
    view:
      # 前缀
      prefix: /view/
      # 后缀
      suffix: .html
```
原来是根路径跳转Security的login接口，现在我们自定义跳转到我们的登录接口
```
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        //默认URL根路径跳转到/login 此路径由SpringSecurity提供
//        registry.addViewController("/").setViewName("redirect:/login");
        registry.addViewController("/").setViewName("redirect:/login-view");
        registry.addViewController("/login-view").setViewName("login");
    }

```
另外：SpringSecurity跨域问题：
1.配置`http.csrf().disable()`
2.在页面登录的Form配置`<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>`

同时需要设置SecurityConfig

```
//允许表单登录
.formLogin()
//登陆页面 跳转自定义的登录页面，如果不设置则如果登录失败 跳转默认Security的登陆页面
.loginPage("/login-view")
//自定义登录页面 login.html 的登录form 提交的 地址
.loginProcessingUrl("/login")
//自定义登录成功的页面地址
.successForwardUrl("/login-success");
```

设置WebConfig
```
registry.addViewController("/").setViewName("redirect:/login-view");
registry.addViewController("/login-view").setViewName("login");  --》/view/login.html

```

# 认证与授权 

<table>
<tr>
<td>用户</td>
<td>角色</td>
<td>权限</td>
<td>可访问资源</td>
</tr>
<tr>
<td>zs</td>
<td>ADMIN</td>
<td>p1、p3</td>
<td>/r/r1、/r/r3</td>
</tr>
<tr>
<td>ls</td>
<td>USER</td>
<td>p2、p4</td>
<td>/r/r2、/r/r4</td>
</tr>
</table>

访问r1 需要p1权限     zs可以访问<br>
访问r2 需要p2权限     ls可以访问<br>
访问r3 需要ADMIN角色  zs可以访问<br>
访问r4 需要USER权限   ls可以访问<br>
注意：角色信息在登陆的时候就已经查询，若请求只检验角色则不需要在查询数据库，但是如果请求需要校验权限，则需要每次都查询数据库权限信息。<br>

登录地址：http://localhost:8080/security/login  <br>
登出地址：http://localhost:8080/security/logout  <br>
登录成功跳转地址：http://localhost:8080/security/login-success   <br>
访问r1权限：http://localhost:8080/security/r/r1  <br>
访问r1权限：http://localhost:8080/security/r/r2  <br>
除了/login请求，访问其他请求都需要认证<br>


```
 @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/r/r1").hasAuthority("p1")//访问/r/r1需要p1的权限
                .antMatchers("/r/r2").hasAuthority("p2")
                .antMatchers("/r/r3").hasAuthority("p3")
                .antMatchers("/r/r4").hasAuthority("p4")
                .antMatchers("/r/**").authenticated()
                .antMatchers("/login-success").authenticated()
                .anyRequest().permitAll()//除了以上需要认证的请求，其他请求都可以访问
                .and()
                //允许表单登录
                .formLogin()
                //自定义登录成功的页面地址
                .successForwardUrl("/login-success");
    }
```
状态码403认证授权失败


建表SQL：
```
CREATE TABLE `sys_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `sys_role` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `sys_user_role` (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `fk_role_id` (`role_id`),
  CONSTRAINT `fk_role_id` FOREIGN KEY (`role_id`) REFERENCES `sys_role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `sys_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `sys_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) DEFAULT NULL,
  `permission` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

CREATE TABLE `sys_role_permission` (
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`role_id`,`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `sys_user`(`id`, `name`, `password`) VALUES (1, 'zs', '$2a$10$FFYhLLG2LWqkIHo0KgeRbuO1xt93cFn7Zce0TzmQY545uJcEL6Hgu');
INSERT INTO `sys_user`(`id`, `name`, `password`) VALUES (2, 'ls', '$2a$10$XJqba53AOaj2oy1kmjjAF.V8rwjdxXGwM5laYsidzxlt4MvyCcj0u');
INSERT INTO `sys_user`(`id`, `name`, `password`) VALUES (3, 'ww', '$2a$10$FFYhLLG2LWqkIHo0KgeRbuO1xt93cFn7Zce0TzmQY545uJcEL6Hgu');

INSERT INTO `sys_user_role`(`user_id`, `role_id`) VALUES (1, 1);
INSERT INTO `sys_user_role`(`user_id`, `role_id`) VALUES (2, 2);


INSERT INTO `sys_role` VALUES ('1', 'ROLE_ADMIN');
INSERT INTO `sys_role` VALUES ('2', 'ROLE_USER');

INSERT INTO `sys_role_permission`(`role_id`, `permission_id`) VALUES (1, 1);
INSERT INTO `sys_role_permission`(`role_id`, `permission_id`) VALUES (1, 3);
INSERT INTO `sys_role_permission`(`role_id`, `permission_id`) VALUES (2, 2);
INSERT INTO `sys_role_permission`(`role_id`, `permission_id`) VALUES (2, 4);


INSERT INTO `sys_permission`(`id`, `url`, `permission`) VALUES (1, '/r/r1', 'p1');
INSERT INTO `sys_permission`(`id`, `url`, `permission`) VALUES (2, '/r/r2', 'p2');
INSERT INTO `sys_permission`(`id`, `url`, `permission`) VALUES (3, '/r/r3', 'p3');
INSERT INTO `sys_permission`(`id`, `url`, `permission`) VALUES (4, '/r/r4', 'p4');

```

# 加解密
SpringSecurity默认使用`Bcrypt`进行加密解密<br>
`BCryptPasswordEncoder`类提供了`encode`方法进行加密, `matches`进行解密校验<br>
`BCrypt.gensalt()` 获得盐<br>
也可以 `BCrypt.hashpw("123", BCrypt.gensalt())` 进行加盐加密<br>
使用 `BCrypt.checkpw("123", hashpw1)` 进行校验<br>
每次加密后的值不一样，但是都可以校验成功<br>


