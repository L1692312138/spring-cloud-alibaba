package com.lsh.order.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/8/20 12:46 下午
 * @desc ：Bean配置类
 */
@Configuration
public class TokenConfig {


    /**
     * 令牌存储方案  使用内存
     * @return
     */
//    @Bean
//    public TokenStore tokenStore(){
//        //使用内存存储令牌 （普通令牌）
//        return new InMemoryTokenStore();
//    }
    /**
     * 令牌存储方案  使用JWT格式
     * @return
     */
    @Bean
    public TokenStore tokenStore(){
        return new JwtTokenStore(accessTokenConverter());
    }

    /**使用对称加密*/
    public  String SigningKey = "oauth1234";

    /**
     * 使用JWT令牌格式
     * @return
     */
//    @Bean
//    public JwtAccessTokenConverter accessTokenConverter(){
//        //使用JWT存储令牌 （普通令牌）
//        JwtAccessTokenConverter jwtAccessTokenConverter = new JwtAccessTokenConverter();
//        //对称秘钥 资源服务器使用该秘钥进行验证
//        jwtAccessTokenConverter.setSigningKey(SigningKey);
//        return jwtAccessTokenConverter;
//    }


    /**存放公钥的文件名 */
    public String first_public = "first_public.txt";
    public String second_public = "second_public.txt";

    /**
     * 使用JWT存储令牌   RSA非对称加密
     *
     * 使用私钥 在认证服务器进行加密
     * 使用公钥 在资源服务器解析JWT时解密
     * @return
     */
    @Bean
    public JwtAccessTokenConverter accessTokenConverter(){
        //使用JWT存储令牌 （普通令牌）
        JwtAccessTokenConverter jwtAccessTokenConverter = new JwtAccessTokenConverter();

        //公钥（读取public.txt的公钥信息）
        Resource resource = new ClassPathResource(first_public);
        String publicKey = null;

        try {
            publicKey = inputStream2String(resource.getInputStream());
        } catch (final IOException e) {
            throw new RuntimeException(e);
        }
        //设置验证秘钥（公钥）
        jwtAccessTokenConverter.setVerifierKey(publicKey);

        return jwtAccessTokenConverter;
    }
    public String inputStream2String(InputStream is) throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(is));
        StringBuffer buffer = new StringBuffer();
        String line = "";
        while ((line = in.readLine()) != null) {
            buffer.append(line);
        }
        return buffer.toString();
    }




}
