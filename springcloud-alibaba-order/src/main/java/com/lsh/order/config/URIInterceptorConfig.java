package com.lsh.order.config;

import com.lsh.order.interception.URIInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/7/19 2:23 下午
 * @desc ：
 */
@Configuration
public class URIInterceptorConfig implements WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 将自己的拦截器注册进运行环境中
        InterceptorRegistration registration = registry.addInterceptor(new URIInterceptor());
        // /* 匹配单层路径 /a 或者/b 可以拦截到 ,/a/b/c 这样的多层路径不会拦截
        // /** 匹配单层路径 /a 或者/b 多层路径/a/b/c 都可以
        // 支持后缀匹配.注意不是*.do 而是 /*.do
        // 支持路径中的模糊匹配 /user/**
//        registration.addPathPatterns("/order/**");
        registration.addPathPatterns("/**");
        // 添加一些不拦截路径
//        registration.excludePathPatterns("/login","/logout","/html/t1.html");

    }
}
