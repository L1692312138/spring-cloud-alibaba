package com.lsh.order.service;

import com.lsh.order.entity.Order;

import java.util.List;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/6/29 2:26 下午
 * @desc ：
 */

public interface OrderService {

    List<Order> findAll();

    void create(Order order);

    void update(Integer userId, Integer money, Integer status);

    String decreaseAccount();

}
