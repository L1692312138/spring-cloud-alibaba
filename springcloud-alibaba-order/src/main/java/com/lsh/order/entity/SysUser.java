package com.lsh.order.entity;

import lombok.Data;

import javax.persistence.*;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/8/18 7:53 下午
 * @desc ：
 */
@Data
@Entity
@Table(name = "sys_user")
public class SysUser  {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private String password;

}
