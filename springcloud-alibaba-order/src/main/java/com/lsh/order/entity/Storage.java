package com.lsh.order.entity;

import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/3/22 4:13 下午
 * @desc ：
 */
@Data
@Entity
@DynamicUpdate
@DynamicInsert
@Table(name = "storage")
public class Storage {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**产品id*/
    private Integer productId;

    /**总库存*/
    private Integer total;

    /**已用库存*/
    private Integer used;

    /**剩余库存*/
    private Integer residue;
}
