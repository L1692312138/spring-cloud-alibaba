package com.lsh.order.entity;

import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/3/22 4:06 下午
 * @desc ：
 */
@Data
@Entity
@DynamicUpdate
@DynamicInsert
@Table(name = "goods_order")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private Integer userId;

    private Integer productId;

    private Integer count;
    /**
     * BigDecimal 高精度
     */
    private Integer money;

    /**订单状态：0：创建中；1：已完结*/
    private Integer status;

}
