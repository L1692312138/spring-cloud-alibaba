package com.lsh.order.repository;

import com.lsh.order.entity.SysUser;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/8/18 8:06 下午
 * @desc ：
 */
public interface SysUserRepository extends JpaRepository<SysUser,Integer> {
    // 根据用户名查询用户信息
    SysUser findTopByName(String username);
}
