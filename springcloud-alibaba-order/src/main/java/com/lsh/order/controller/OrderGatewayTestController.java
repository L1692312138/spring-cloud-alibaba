package com.lsh.order.controller;

import com.lsh.common.util.ResultObject;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/8/17 5:02 下午
 * @desc ：
 */
@RestController
@RequestMapping("/order/gateway")
public class OrderGatewayTestController {

    @GetMapping("/test")
    public ResultObject getFindAll(){

        return new ResultObject(true,0000,"order service success",null);
    }
}
