package com.lsh.order.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.lsh.order.entity.Order;
import com.lsh.order.feign.AccountApi;
import com.lsh.order.service.OrderService;
import com.lsh.common.util.ResultObject;
import com.lsh.common.util.StatusCode;
import com.lsh.common.util.SystemConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/6/29 3:42 下午
 * @desc ：
 */
@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    OrderService orderService;

    @Autowired
    AccountApi accountApi;

    @Autowired
    RestTemplate restTemplate;

    /**
     * 1.创建订单
     * 2.扣减库存
     * 3.扣减账户 -> 4.修改订单状态
     *
     * @param order
     * @return
     */
//    @SentinelResource("ORDER-CREATE")
    @GetMapping("/create")
    public ResultObject create(Order order){
        //订单状态设置为 订购中
        order.setStatus(0);
        order.setId(null);
        orderService.create(order);
        return new ResultObject(true, StatusCode.OK, SystemConstants.CREATE_SUCCESS);
    }


    /**
     * 更新订单状态
     * @param userId
     * @param money
     * @param status
     * @return
     */
    @GetMapping("/update")
    public ResultObject update(@RequestParam("userId") Integer userId, @RequestParam("money") Integer money, @RequestParam("status") Integer status){
        orderService.update(userId,money,status);
        return new ResultObject(true, StatusCode.OK, SystemConstants.ORDER_UPDATE_SUCCESS);
    }



    /**
     * 调用账号服务查询所有订单数据  通过RestTemplate  调用
     * @return
     */
    @GetMapping("/findAllAccountByOrderRestTemplate")
    public ResultObject findAllAccountByOrderRestTemplate(){
        String url = "http://springcloud-alibaba-account/account/findAll";
        String forObject = restTemplate.getForObject(url, String.class);
        return new ResultObject(true, StatusCode.OK, SystemConstants.QUERY_SUCCESS,forObject);
    }
    /**
     * 调用账号服务查询所有订单数据  通过  Feign 调用
     * @return
     */
    @GetMapping("/findAllAccountByOrderFeign")
    public ResultObject findAllAccountByOrderFeign(){
        String all = accountApi.findAll();
        return new ResultObject(true, StatusCode.OK, SystemConstants.QUERY_SUCCESS,all);
    }


    /**
     * 查询所有订单数据
     * @return
     */
    @SentinelResource("ORDER-FIND-ALL")
    @GetMapping("/findAll")
    public ResultObject findAll(){
        return new ResultObject(true, StatusCode.OK, SystemConstants.QUERY_SUCCESS,orderService.findAll());
    }



    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN')")
    @GetMapping("/test")
    public ResultObject decreaseAccount(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Object principal = authentication.getPrincipal();
        System.out.println("principal:"+principal);
        authentication.getAuthorities().stream().forEach(x->{
            System.out.println("authorization:"+x.getAuthority());
        });
//        String username = principal.toString();
        String username = authentication.getName();

        return new ResultObject(true, StatusCode.OK, SystemConstants.QUERY_SUCCESS,username+"访问Order服务资源");
    }






}
