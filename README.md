# 一、服务名及端口
springcloud-alibaba-account  账户服务          9000   <br>       
springcloud-alibaba-account  账户服务2         9001   <br>      
springcloud-alibaba-order    订单服务          9002   <br>    
springcloud-alibaba-storage  库存服务          9003   <br>    
api-service                  网关服务          9004   <br>   
springcloud-alibaba-user     用户服务          9005   <br>  
springboot-security          security         8080   <br>   

# 二、分布式事务Seata
seata-server     Seata分布式事务TC事务协调器       8091

seata使用nacos作为配置中心：https://blog.csdn.net/DreamsArchitects/article/details/118368658
## 微服务流程流程
Order下单 -> Storage扣减库存 -> Account扣减金额 -> Order完成订单

如果使用1.1版本以上的seata组名记得用 vgroupMapping，1.0及之前的用vgroup_mapping

# 三、数据库
地址：localhost:3306 <br>
库名：seata    <br>
用户名：root  <br>
密码：123456<br>
# 四、Nacos注册中心及配置中心
http://localhost:8848/nacos  <br>
账号：nacos <br>
密码：nacos <br>
Nacos注册中心、配置中心的使用：https://blog.csdn.net/DreamsArchitects/article/details/116790743 <br>
Nacos负载均衡的使用：https://blog.csdn.net/DreamsArchitects/article/details/119034848 <br>
# 五、Sentinel控制台
http://localhost:8888   <br>
账号：sentinel <br>
密码：sentinel <br>
Sentinel与SpringBoot整合:https://blog.csdn.net/DreamsArchitects/article/details/118293490 <br>

# 六、负载均衡
如果使用了负载均衡，则可以直接通过服务名进行访问，代替 ip+port 访问。
## RestTemplate
在Nacos的依赖中集成了Ribbon的依赖 <br>
使用`@LoadBanlans`注解,实现负载均衡的目的。<br>

## Feign
在Feign的依赖中集成了Ribbon的依赖 <br>
使用Feign进行服务调用不需要在加上`@LoadBanlans`注解。

# 七、Gateway网关
路由转发<br>  
api-service  网关服务    9004   <br>  
 
账号服务接口
http://localhost:9000/account/findAll
走网关接口访问账号服务：
http://localhost:9003/springcloud-alibaba-account/account/findAll


# 八、SpringBoot RestTemplate 服务调用
##  GET

getForEntity 方法:<br>
GET请求带参数 http://localhost:9000/account/get?name={1}&age={2},第一个参数是 url ，url 中有一个占位符 {1} ,如果有多个占位<br>
restTemplate.getForObject(url, String.class,name,age)<hr>
第二种 使用{name}占位符  使用Map进行传参 key为name:<br>
String url2 = "http://localhost:9000/account/get?name={name}&age={age}"<br>
String forObject = restTemplate.getForObject(url, String.class,map);
## POST（包括key/value和JSON格式）
唯一的区别就是第二个参数的类型不同，这个参数如果是一个 `MultiValueMap` 的实例，则以 key/value 的形式发送，如果是一个普通对象，则会被转成 json 发送
``` 
MultiValueMap map = new LinkedMultiValueMap();
map.add("name","zhangsan");
map.add("age",20);
```

## PUT（包括key/value和JSON格式）
与 POST 相同，PUT和DELETE没有返回值
## DELETE（参数请求与GET一样）
与GET请求相同。
## Exchange 
构造请求头 `HttpHeaders`
```
HttpHeaders headers = new HttpHeaders();
headers.add("username","ZhangSan");
```
构造请求体 `HttpEntity`
```
//第一个参数实际上就相当于 POST/PUT 请求中的第二个参数；第二个参数是请求头
HttpEntity<Map<String, Object>> httpEntity = new HttpEntity<>(null,headers);

String url1 = "http://localhost:9000/account/get?name={1}&age={2}";
ResponseEntity<String> exchange = restTemplate.exchange(url1, HttpMethod.GET, httpEntity, String.class,name,age);

ResponseEntity<String> exchange2 = restTemplate.exchange(url2, HttpMethod.POST, httpEntity2, String.class);

ResponseEntity<String> exchange3 = restTemplate.exchange(url3, HttpMethod.POST, httpEntity3, String.class);

ResponseEntity<String> exchange4 = restTemplate.exchange(url4, HttpMethod.PUT, httpEntity2,String.class);

ResponseEntity<String> exchange5 = restTemplate.exchange(url5, HttpMethod.PUT, httpEntity3,String.class);

ResponseEntity<String> exchange6 = restTemplate.exchange(url6, HttpMethod.DELETE, httpEntity3, String.class,hashMap);

```
# 九、SpringSecurityOAuth2.0分布式系统认证方案

单体项目可以直接通过SpringSecurity来实现登陆认证授权，
但是在分布式系统中，会有多个服务，如果都实现一套登陆认证逻辑就会非常的冗余，所以将登录认证逻辑独立出来，统一认证服务
## 统一认证服务
提供独立的认证服务，统一进行认证授权逻辑

1.基于Session的认证  不是很适用分布式系统
2.基于Token的认证    
## SPringSecurity
### 认证
通过实现UserDetailService接口，实现自定义认证逻辑

### 授权
通过实现PermissionEvaluator接口，实现更细粒度的权限校验

## SpringSecurityOAuth2
- 授权码模式
- 密码模式
- 简单模式
- 客户端模式

### 令牌服务端点
如果认证服务器用户没有登录认证则需要登录认证<br>
申请授权码GET：http://localhost:8081/oauth2/oauth/authorize?client_id=c1&client_secret=order&response_type=code&scope=ALL&redirect_uri=http://www.baidu.com<br>
授权码模式申请令牌POST：http://localhost:8081/oauth2/oauth/token   grant_type=authorization_code<br>
密码模式申请令牌POST：http://localhost:8081/oauth2/oauth/token不需要申请授权码 grant_type=password 需要username、password信息<br>
简单模式申请令牌：http://localhost:8081/oauth2/oauth/authorize?client_id=c1&client_secret=secret&response_type=token&scope=all&redirect_uri=http://www.baidu.com<br>
申请令牌POST：http://localhost:8081/oauth2/oauth/token<br>
验证令牌GET：http://localhost:9001/oauth/check_token?token= [access_token]<br>
更新令牌GET：http://localhost:8081/oauth2/oauth/token<br>
获取公钥GET：http://localhost:8081/oauth2/oauth/token_key<br>
退出登录：http://localhost:8081/oauth2/login?logout<br>
# SpringCloud Sleuth
Spring-Cloud-Sleuth是Spring Cloud的组成部分之一，为SpringCloud应用实现了一种分布式追踪解决方案，其兼容了Zipkin, HTrace和log-based追踪。<br>
微服务跟踪(sleuth)其实是一个工具,<br>
它在整个分布式系统中能跟踪一个用户请求的过程(包括数据采集，数据传输，数据存储，数据分析，数据可视化)，<br>
捕获这些跟踪数据，就能构建微服务的整个调用链的视图，这是调试和监控微服务的关键工具。<br>


<table>
<tr>
<td>特点</td>
<td>说明</td>
</tr>
<tr>
<td>提供链路追踪</td>
<td>通过sleuth可以很清楚的看出一个请求经过了哪些服务，
    可以方便的理清服务局的调用关系</td>
</tr>
<tr>
<td>性能分析</td>
<td>通过sleuth可以很方便的看出每个采集请求的耗时，
    分析出哪些服务调用比较耗时，当服务调用的耗时
    随着请求量的增大而增大时，也可以对服务的扩容提
    供一定的提醒作用</td>
</tr>
<tr>
<td>数据分析
    优化链路</td>
<td>对于频繁地调用一个服务，或者并行地调用等，
    可以针对业务做一些优化措施</td>
</tr>
<tr>
<td>可视化</td>
<td>对于程序未捕获的异常，可以在zipkpin界面上看到</td>
</tr>

</table>

## Zipkin

nohup java -jar zipkin-server-2.9.3-exec.jar &
http://127.0.0.1:9411/ 

# RabbitMQ

涉及服务：Account 、Storage

# 消息确认机制
Producer发送 消息 到 Broker  会触发ConfirmCallBack确认模式<br>
如果Broker没有投入到正确的队列里 会触发ReturnCallBack退回模式（如果投到了就不会触发）<br>
以及消息接收会有消息接收确认<br>

# Docker

将应用打包成Docker容器部署

命令行运行：
pom文件配置一下配置：`mvn clean package  -Dmaven.test.skip=true docker:build -DpushImage`
```
<build>
        <finalName>springcloud-alibaba-test</finalName>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
            </plugin>
            <!-- docker的maven插件，官网 https://github.com/spotify/docker-maven-plugin -->
            <plugin>
                <groupId>com.spotify</groupId>
                <artifactId>docker-maven-plugin</artifactId>
                <version>1.0.0</version>
                <configuration>
                    <imageName>116.62.13.104:5000/${project.artifactId}:${project.version}</imageName>
                    <baseImage>jdk1.8</baseImage>
                    <entryPoint>["java", "-jar","/${project.build.finalName}.jar"]</entryPoint>
                    <resources>
                        <resource>
                            <targetPath>/</targetPath>
                            <directory>${project.build.directory}</directory>
                            <include>${project.build.finalName}.jar</include>
                        </resource>
                    </resources>
                    <dockerHost>http://116.62.13.104:2375</dockerHost>
                </configuration>
            </plugin>
        </plugins>
    </build>

```

# POI导入导出
<table><thead><tr><th>参数</th> <th>类型</th> <th>默认值</th> <th>描述</th></tr></thead> <tbody><tr><td>sort</td> <td>int</td> <td>Integer.MAX_VALUE</td> <td>导出时在excel中排序</td></tr> <tr><td>name</td> <td>String</td> <td>空</td> <td>导出到Excel中的名字</td></tr> <tr><td>dateFormat</td> <td>String</td> <td>空</td> <td>日期格式, 如: yyyy-MM-dd</td></tr> <tr><td>dictType</td> <td>String</td> <td>空</td> <td>如果是字典类型，请设置字典的type值 (如: sys_user_sex)</td></tr> <tr><td>readConverterExp</td> <td>String</td> <td>空</td> <td>读取内容转表达式 (如: 0=男,1=女,2=未知)</td></tr> <tr><td>separator</td> <td>String</td> <td>,</td> <td>分隔符，读取字符串组内容</td></tr> <tr><td>scale</td> <td>int</td> <td>-1</td> <td>BigDecimal 精度 默认:-1(默认不开启BigDecimal格式化)</td></tr> <tr><td>roundingMode</td> <td>int</td> <td>BigDecimal.ROUND_HALF_EVEN</td> <td>BigDecimal 舍入规则 默认:BigDecimal.ROUND_HALF_EVEN</td></tr> <tr><td>columnType</td> <td>Enum</td> <td>Type.STRING</td> <td>导出类型（0数字 1字符串 2图片）</td></tr> <tr><td>height</td> <td>String</td> <td>14</td> <td>导出时在excel中每个列的高度 单位为字符</td></tr> <tr><td>width</td> <td>String</td> <td>16</td> <td>导出时在excel中每个列的宽 单位为字符</td></tr> <tr><td>suffix</td> <td>String</td> <td>空</td> <td>文字后缀,如% 90 变成90%</td></tr> <tr><td>defaultValue</td> <td>String</td> <td>空</td> <td>当值为空时,字段的默认值</td></tr> <tr><td>prompt</td> <td>String</td> <td>空</td> <td>提示信息</td></tr> <tr><td>combo</td> <td>String</td> <td>Null</td> <td>设置只能选择不能输入的列内容</td></tr> <tr><td>targetAttr</td> <td>String</td> <td>空</td> <td>另一个类中的属性名称,支持多级获取,以小数点隔开</td></tr> <tr><td>isStatistics</td> <td>boolean</td> <td>false</td> <td>是否自动统计数据,在最后追加一行统计数据总和</td></tr> <tr><td>type</td> <td>Enum</td> <td>Type.ALL</td> <td>字段类型（0：导出导入；1：仅导出；2：仅导入）</td></tr></tbody></table>

## 导出Excel
1. 自定义注解@Excel
2. ExcelUtil 通过类获取字段上注解，创建工作簿Workbook、工作表Sheet、字段Field、样式Style、Cell单元格

## 导入Excel







