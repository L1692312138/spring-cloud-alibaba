package com.lsh.common.enums;

/**
 * 操作人类别
 * @Date 2021年09月24日08:58:45
 * @Author LiuShihao
 */
public enum OperatorType {
    /**
     * 其它
     */
    OTHER,

    /**
     * 后台用户
     */
    MANAGE,

    /**
     * 手机端用户
     */
    MOBILE
}
