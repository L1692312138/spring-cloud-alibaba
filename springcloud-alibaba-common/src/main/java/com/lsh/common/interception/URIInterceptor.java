package com.lsh.common.interception;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/7/19 2:21 下午
 * @desc ：拦截器 打印请求路径
 */
@Component
public class URIInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        ServletContext servletContext = request.getServletContext();

        System.out.println("URIInterceptor - url："+request.getRequestURL());
        return true;
    }
}
