package com.lsh.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/9/15 2:21 下午
 * @desc ：
 */
@SpringBootTest
public class StreamTest {
    /**
     * 取出重复字段的对象
     */
    @Test
    public void test1(){
        Car car1 = new Car(1, "宝马", "1.00");
        Car car2 = new Car(2, "奔驰", "1.00");
        Car car3 = new Car(3, "奥迪", "1.00");
        Car car4 = new Car(4, "领克03", "0.01");
        Car car5 = new Car(5, "领克03", "0.01");

        List<Car> cars1 = Arrays.asList(car1,car2, car3,car5);
        List<Car> cars2 = Arrays.asList(car2, car4);

        ArrayList<Car> cars = new ArrayList<>();
        cars1.stream().map(x -> {
            cars2.stream().map(y ->{
                if (x.getName().equals(y.getName()) && x.getPrice().equals(y.getPrice())){
                    cars.add(x);
                }
                return y;}).collect(Collectors.toList());
            return x; }).collect(Collectors.toList());
        System.out.println(cars);
    }
    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    class Car{
        private Integer id;
        private String name;
        private String price;
    }
}
