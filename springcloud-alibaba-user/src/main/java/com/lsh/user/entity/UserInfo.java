package com.lsh.user.entity;

import com.lsh.common.annotation.Excel;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/9/14 3:33 下午
 * @desc ：
 */
@Data
@Entity(name = "user_info")
public class UserInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id ;
    @Excel(name = "用户名")
    private String userName;
    @Excel(name = "用户手机号码")
    private String userPhone;
    @Excel(name = "用户年龄")
    private Integer userAge;
    @Excel(name = "用户生日")
    private String userBirthday;
    @Excel(name = "创建日期", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    @Excel(name = "更新日期", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
    @Excel(name = "删除日期", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date deleteTime;
    @Excel(name = "删除标识", readConverterExp = "0=已删除,1=在网")
    private int flag;
}
