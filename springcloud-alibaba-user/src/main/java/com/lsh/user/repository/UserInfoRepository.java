package com.lsh.user.repository;

import com.lsh.user.entity.UserInfo;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/9/14 4:43 下午
 * @desc ：
 */
public interface UserInfoRepository  extends JpaRepository<UserInfo,Integer> {
}
