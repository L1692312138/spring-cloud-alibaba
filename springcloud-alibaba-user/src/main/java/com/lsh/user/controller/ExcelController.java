package com.lsh.user.controller;

import com.lsh.common.poi.ExcelUtil;
import com.lsh.common.util.AjaxResult;
import com.lsh.user.entity.UserInfo;
import com.lsh.user.repository.UserInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/9/14 3:47 下午
 * @desc ：
 */
@RestController
public class ExcelController {

    @Autowired
    UserInfoRepository userInfoRepository;

    @GetMapping("/excel/allUser")
    public AjaxResult ExcelAllUser(HttpServletResponse response) throws IOException {
        List<UserInfo> all = userInfoRepository.findAll();
        System.out.println("用户人数："+all.size());
        ExcelUtil<UserInfo> util = new ExcelUtil<UserInfo>(UserInfo.class);
        System.out.println("Excel已导出。");
        //Excel输出到本地
        return util.exportExcel(all, "用户数据");


//        String filename = (String)ajaxResult.get("msg");
//        File file = new File(filename);
//        FileInputStream fis = new FileInputStream(file);

//        // 解决响应中文文件名乱码问题
//        filename = URLEncoder.encode(filename, "utf-8");
//        // 浏览器响应下载弹框
//        response.setHeader("Content-disposition", "attachment;filename=" + filename );
//        response.setContentType("application/msexcel");
//        // 输出
//        OutputStream out = response.getOutputStream();
//        byte[] bytes = new byte[1024];
//        while (fis.read(bytes) != -1){
//            out.write(bytes);
//        }
//        fis.close();
//        out.close();


    }
}
