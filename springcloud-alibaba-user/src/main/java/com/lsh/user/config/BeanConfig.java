package com.lsh.user.config;

import com.lsh.common.redis.RedisCache;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/9/15 9:04 上午
 * @desc ：
 */
@Configuration
public class BeanConfig {
    @Bean
    public RedisCache redisCache(){
        return new RedisCache();
    }

    @Bean
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }
}
