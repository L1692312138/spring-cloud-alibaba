package com.lsh.user.dto;

import lombok.Data;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/9/17 9:45 上午
 * @desc ：
 */
@Data
public class LoginDto {
    /**
     * 用户名
     */
    private String username;

    /**
     * 用户密码
     */
    private String password;

    /**
     * 验证码
     */
    private String code;

    /**
     * 唯一标识
     */
    private String uuid = "";
}
